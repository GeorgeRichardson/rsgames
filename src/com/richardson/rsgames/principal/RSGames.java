package com.richardson.rsgames.principal;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.richardson.rsgames.managers.pojos.ConsoleManager;
import com.richardson.rsgames.managers.pojos.GeneroManager;
import com.richardson.rsgames.managers.pojos.JogoManager;
import com.richardson.rsgames.utils.Util;

public class RSGames {

	public static void main(String[] args) {
		while (true) {
			System.out.println("\n\n\n");
			System.out.println(".:Menu de Op��es:.");
			System.out.println("------------------");
			System.out.println("1 - Listar Consoles");
			System.out.println("2 - Listar G�neros");
			System.out.println("3 - Listar Jogos");
			System.out.println("9 - Sair");
			System.out.print("Informe sua op��o: ");

			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			
			String entrada;

			try {
				entrada = br.readLine();
				if (Util.tryParse(entrada)) {
					switch (Integer.parseInt(entrada)) {
					case 1:
						ConsoleManager cm = new ConsoleManager();
						cm.listarConsoles();
						break;
					case 2:
						GeneroManager gm = new GeneroManager();
						gm.listarGeneros();
						break;
					case 3:
						JogoManager jm = new JogoManager();
						jm.listarJogos();
						break;
					case 9:
						System.out.println("Fui!");
						System.exit(0);
						break;
					default:
						System.out.println("Op��o inv�lida....");
					}
				}
			} catch (Exception ex) {
				System.out.println("Um erro ocorreu...");
				System.out.println(ex.getMessage());
			}

		}
	}
}
