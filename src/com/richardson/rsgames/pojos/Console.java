package com.richardson.rsgames.pojos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * @author George Richardson
 *
 */
@Entity
@Table(name = "CONSOLES", schema = "RSGAMES")
public class Console implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "sequence", sequenceName = "SEQ_CONSOLES", schema = "RSGAMES", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence")
	@Column(name = "ID_CONSOLE")
	private long idConsole;

	@Column(name = "DS_CONSOLE")
	private String dsConsole;

	public long getIdConsole() {
		return idConsole;
	}

	public void setIdConsole(long idConsole) {
		this.idConsole = idConsole;
	}

	public String getDsConsole() {
		return dsConsole;
	}

	public void setDsConsole(String dsConsole) {
		this.dsConsole = dsConsole;
	}
}