package com.richardson.rsgames.pojos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * @author George Richardson
 *
 */
@Entity
@Table(name = "JOGOS", schema = "RSGAMES")
public class Jogo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "sequence", sequenceName = "SEQ_JOGOS", schema = "RSGAMES", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence")
	@Column(name = "ID_JOGO")
	private long idJogo;

	@Column(name = "DS_JOGO")
	private String dsJogo;
	
    @OneToOne
    @JoinColumn(name="ID_CONSOLE")	
	private Console console;
	
    @OneToOne
    @JoinColumn(name="ID_GENERO")
	private Genero genero;

	public long getIdJogo() {
		return idJogo;
	}

	public void setIdJogo(long idJogo) {
		this.idJogo = idJogo;
	}

	public String getDsJogo() {
		return dsJogo;
	}

	public void setDsJogo(String dsJogo) {
		this.dsJogo = dsJogo;
	}

	public Console getConsole() {
		return console;
	}

	public void setConsole(Console console) {
		this.console = console;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public Jogo(){
		this.console = new Console();
		this.genero = new Genero();
	}
}
