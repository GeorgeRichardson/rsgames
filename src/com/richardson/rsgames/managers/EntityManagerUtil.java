package com.richardson.rsgames.managers;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerUtil {
	private static final EntityManagerFactory entityManagerFactory;
	static {
		try {
			System.out.println("Estabelecendo conex�o com o H2 DB...");
			entityManagerFactory = Persistence.createEntityManagerFactory("test");
			System.out.println("Conex�o com o H2 DB estabelecida com sucesso!");
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed: " + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();

	}
}