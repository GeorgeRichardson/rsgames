package com.richardson.rsgames.managers.pojos;

import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;

import com.richardson.rsgames.managers.EntityManagerUtil;
import com.richardson.rsgames.pojos.Console;
import com.richardson.rsgames.utils.Util;

public class ConsoleManager {
	private EntityManager em;

	public ConsoleManager() throws Exception {
		try {
			em = EntityManagerUtil.getEntityManager();
		} catch (Exception ex) {
			throw new Exception("Erro ao definir EntityManager: "
					+ ex.getMessage() + " -- " + ex.getStackTrace().toString());
		}
	}

	public void listarConsoles() throws Exception {
		try {
			em.getTransaction().begin();
			@SuppressWarnings("unchecked")
			List<Console> consoles = em.createQuery("from Console")
					.getResultList();
			if (!consoles.isEmpty()) {
				System.out.println("Consoles cadastrados:");
				System.out.println("---------------------");
				for (Iterator<Console> iterator = consoles.iterator(); iterator.hasNext();) {
					Console c = (Console) iterator.next();
					System.out.println("ID_CONSOLE: " + Util.textPad(Long.toString(c.getIdConsole()), 3, '0', true) + " - " + 
					                   "DS_CONSOLE: " + Util.textPad(c.getDsConsole(), 30, ' ', false));
				}
			} else {
				System.out.println("N�o h� Consoles cadastrados.");
			}
			em.getTransaction().commit();
		} catch (Exception ex) {
			em.getTransaction().rollback();
			throw new Exception("Erro ao listar Consoles: " + ex.getMessage()
					+ " -- " + ex.getStackTrace().toString());
		}
	}
}