package com.richardson.rsgames.managers.pojos;

import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;

import com.richardson.rsgames.managers.EntityManagerUtil;
import com.richardson.rsgames.pojos.Jogo;
import com.richardson.rsgames.utils.Util;

public class JogoManager {
	private EntityManager em;

	public JogoManager() throws Exception {
		try {
			em = EntityManagerUtil.getEntityManager();
		} catch (Exception ex) {
			throw new Exception("Erro ao definir EntityManager: "
					+ ex.getMessage() + " -- " + ex.getStackTrace().toString());
		}
	}

	public void listarJogos() throws Exception {
		try {
			em.getTransaction().begin();
			@SuppressWarnings("unchecked")
			List<Jogo> jogos = em.createQuery("from Jogo").getResultList();
			if (!jogos.isEmpty()) {
				System.out.println("Jogos cadastrados:");
				System.out.println("------------------");
				for (Iterator<Jogo> iterator = jogos.iterator(); iterator.hasNext();) {
					Jogo j = (Jogo) iterator.next();
					System.out.println("ID_JOGO: " + Util.textPad(Long.toString(j.getIdJogo()), 3, '0', true) + " - " + 
					                   "DS_JOGO: " + Util.textPad(j.getDsJogo(), 50, ' ', false) + " - " + 
							           "ID_CONSOLE: " + Util.textPad(Long.toString(j.getConsole().getIdConsole()), 3, '0', true) + " - " + 
					                   "DS_CONSOLE: " + Util.textPad(j.getConsole().getDsConsole(), 30, ' ', false) + " - " + 
							           "ID_GENERO: " + Util.textPad(Long.toString(j.getGenero().getIdGenero()), 3, '0', true) + " - " + 
					                   "DS_GENERO: " + Util.textPad(j.getGenero().getDsGenero(), 15, ' ', false));
				}
			} else {
				System.out.println("N�o h� Jogos cadastrados.");
			}
			em.getTransaction().commit();
		} catch (Exception ex) {
			em.getTransaction().rollback();
			throw new Exception("Erro ao listar Jogos: " + ex.getMessage() + " -- " + ex.getStackTrace().toString());
		}
	}
}
