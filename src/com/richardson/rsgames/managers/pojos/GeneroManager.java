package com.richardson.rsgames.managers.pojos;

import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;

import com.richardson.rsgames.managers.EntityManagerUtil;
import com.richardson.rsgames.pojos.Genero;
import com.richardson.rsgames.utils.Util;

public class GeneroManager {
	private EntityManager em;

	public GeneroManager() throws Exception {
		try {
			em = EntityManagerUtil.getEntityManager();
		} catch (Exception ex) {
			throw new Exception("Erro ao definir EntityManager: "
					+ ex.getMessage() + " -- " + ex.getStackTrace().toString());
		}
	}

	public void listarGeneros() throws Exception {
		try {
			em.getTransaction().begin();
			@SuppressWarnings("unchecked")
			List<Genero> generos = em.createQuery("from Genero").getResultList();
			if (!generos.isEmpty()) {
				System.out.println("G�neros cadastrados:");
				System.out.println("---------------------");
				for (Iterator<Genero> iterator = generos.iterator(); iterator.hasNext();) {
					Genero g = (Genero) iterator.next();
					System.out.println("ID_GENERO: " + Util.textPad(Long.toString(g.getIdGenero()), 3, '0', true) + " - " + 
					                   "DS_GENERO: " + Util.textPad(g.getDsGenero(), 15, ' ', false));
				}
			} else {
				System.out.println("N�o h� G�neros cadastrados.");
			}
			em.getTransaction().commit();
		} catch (Exception ex) {
			em.getTransaction().rollback();
			throw new Exception("Erro ao listar G�neros: " + ex.getMessage() + " -- " + ex.getStackTrace().toString());
		}
	}
}
