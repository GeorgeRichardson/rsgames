package com.richardson.rsgames.utils;

public class Util {
	public static boolean tryParse(String valor){
		boolean result = false;
		try {
			Integer.parseInt(valor);
			result = true;
		} catch (NumberFormatException nfe){
			result = false;
		} catch (Exception ex){
			
		}
		
		return result;
	}
	
	public static String textPad(String texto, int tamanho, char caractere, boolean pelaEsquerda){
		String resultado = null;
		try {
			if (!pelaEsquerda){
				resultado = String.format("%1$-" + tamanho + "s", texto).replace(' ', caractere);
			} else {
				resultado = String.format("%1$" + tamanho + "s", texto).replace(' ', caractere);
			}
		} catch (Exception ex){
			
		}
		return resultado;
	}
}
