# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repo contains a Game List App

### How do I get set up? ###

You will need:

- Java 1.8 SE
- H2 DB
- antlr-2.7.7
- classmate-1.3.0
- dom4j-1.6.1
- h2-1.4.192
- hibernate-commons-annotations-5.0.1.Final
- hibernate-core-5.1.0.Final
- hibernate-entitymanager-5.1.0.Final
- hibernate-jpa-2.1-api-1.0.0.Final
- javassist-3.20.0-GA
- jboss-logging-3.3.0.Final
- jboss-transaction-api_1.1_spec-1.0.1.Final

- h2-setup-2016-10-31.exe

### H2 DB Scripts ###
Must be executed in the following order:

- create_user.sql
- create_schema.sql
- grant_rights.sql
- create_table_consoles.sql
- create_table_generos.sql
- create_table_jogos.sql
- alter_table_jogos_fk_consoles.sql
- alter_table_jogos_fk_generos.sql
- populate_generos.sql
- populate_consoles.sql
- populate_jogos.sql

### Who do I talk to? ###

* George Richardson: georgefdrichardson@gmail.com